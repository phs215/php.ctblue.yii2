<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 1/28/2019
 * Time: 8:53 PM
 */

namespace ctblue\yii2\runtime;


class Log
{
    public static function addToLog($object, $filename = 'test')
    {
        ob_start();
        var_dump($object);
        $a = ob_get_clean();
        file_put_contents(\Yii::getAlias('@runtime') . '/logs/' . $filename, $a);
    }
}