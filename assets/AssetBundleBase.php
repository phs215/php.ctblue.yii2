<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 12/7/2019
 * Time: 11:56 AM
 */

namespace ctblue\yii2\assets;


use yii\web\AssetBundle;

class AssetBundleBase extends AssetBundle
{
    public function __construct(array $config = [])
    {
        if(!\Yii::getAlias('@npmroot')){
            throw new \Exception("@npmroot is not defined. It should point to node-modules and should be defined in the common/main.php");
        }
        parent::__construct($config);
    }
}