<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 12/7/2019
 * Time: 1:55 PM
 */

namespace ctblue\yii2\assets;


class AutoCompleteAsset extends AssetBundleBase
{
    public $js = [
        'autocomplete.js'
    ];
    public $css = [
        'autocomplete.css'
    ];

    public function __construct(array $config = [])
    {
        $this->sourcePath = __DIR__ . '/autocomplete/';
        parent::__construct($config);
    }
}