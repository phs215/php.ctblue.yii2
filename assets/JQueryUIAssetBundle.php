<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 12/7/2019
 * Time: 11:42 AM
 */

namespace ctblue\yii2\assets;


use yii\web\AssetBundle;

class JQueryUIAssetBundle extends AssetBundleBase
{
    public $sourcePath = '@npmroot/jquery-ui-dist/';
    public $css = [
        'jquery-ui.min.css',
    ];
    public $js = [
        'jquery-ui.min.js'
    ];
}
