function autocomplete(inp, arr, btnAll, changeCallback, displayCount, leaveCallback) {
    if (displayCount == undefined) {
        displayCount = 10;
    }
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    $(btnAll).click(function () {
        let autocomplete_list = $('#' + $(inp).attr('id') + "autocomplete-list");
        if ($(autocomplete_list).length > 0) {//show and hide the autocomplete list with the drop down button
            closeAllLists();
        } else {
            show('');
        }
    })

    function show(val) {
        let a, b;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", $(inp).attr('id') + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        $(inp).parent().append(a);
        const keys = Object.keys(arr);
        let countValid = 0;
        var me = this;
        for (let i = 0; i < keys.length; i++) {
            const key = keys[i];
            /*check if the item starts with the same letters as the text field value:*/
            const index = arr[key].toUpperCase().indexOf(val.toUpperCase());
            if (index > -1 || !val) {
                if (countValid >= displayCount) continue;
                countValid++;
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML = arr[key].substr(0, index);
                b.innerHTML += "<b>" + arr[key].substr(index, val.length) + "</b>";
                b.innerHTML += arr[key].substr(index + val.length, arr[key].length);
                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + arr[key] + "' key='" + key + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function (e) {
                    inp.value = $(this).find('input').val();
                    let key = $(this).find('input').attr('key');
                    changeCallback(inp.value, key);
                    closeAllLists();
                });
                $(a).append(b);
            }
        }
    }

    $(inp).keyup(function () {
        let a, b, i, val = this.value;
        changeCallback(this.value);
        if (!val) {
            closeAllLists();
            return;
        }
        show(val);
    });

    function closeAllLists(elmnt) {
        $(".autocomplete-items").remove();
    }

    $("body").on('click', function (e) {
        if ($(e.target).parents('.autocomplete').length == 0) {
            closeAllLists(e.target);
            if (leaveCallback != undefined && leaveCallback) {
                leaveCallback();
            }
        }
    })
}