<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 11/12/2019
 * Time: 1:47 PM
 */
namespace ctblue\yii2\views;

class LayoutDesigner
{
    public static function removeAutocomplete()
    {
        echo '<script>$("form").attr("autocomplete","off");</script>';
    }

    public static function tooltipOnHref()
    {
        ?>
        <script>
            $(function(){
                $('a').each(function () {
                    if ($(this)[0].hasAttribute('title')) $(this).attr("data-toggle", 'tooltip');
                });
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
        <?php
    }
}