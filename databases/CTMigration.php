<?php

namespace ctblue\yii2\databases;


use yii\db\Migration;

class CTMigration extends Migration
{
    /**
     * @param string|array $cmd
     */
    public function executeMulti($cmd)
    {
        DbUtils::execute(\Yii::$app->db, $cmd);
    }
}