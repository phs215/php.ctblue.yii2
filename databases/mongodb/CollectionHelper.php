<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 5/29/2019
 * Time: 12:48 AM
 */

namespace ctblue\yii2\databases\mongodb;


class CollectionHelper
{
    public static function collectionExists($db, $collectionName)
    {
//        var_dump($db->createCommand(['show collections;'])->execute());
        $collections = $db->database->listCollections();
        foreach ($collections as $collection) {
            if ($collection['name'] == $collectionName) return true;
        }
        return false;
    }

    public static function createCollection($db, $collection)
    {
        if(!CollectionHelper::collectionExists($db,$collection)){
            return $db->database->createCollection($collection);
        }else{
            throw new \Exception("Collection $collection already exists");
        }
    }
    public static function dropCollection($db, $collection)
    {
        if(CollectionHelper::collectionExists($db,$collection)){
            return $db->database->dropCollection($collection);
        }else{
            throw new \Exception("Collection $collection does not exists");
        }
    }

    public static function addColumn($db, $collection, $column, $defaultValue)
    {
        if (CollectionHelper::collectionExists($db, $collection)) {
            $collection = $db->getCollection($collection);
            $collection->update([], ['$set' => [$column => $defaultValue]]);
        } else {
            throw new Exception("Collection $collection does not exist");
        }
    }

    public static function dropColumn($db, $collection, $column)
    {
        if (CollectionHelper::collectionExists($db, $collection)) {
            $collection = $db->getCollection($collection);
            $collection->update([], ['$unset' => [$column => '']]);
        } else {
            throw new Exception("Collection $collection does not exist");
        }
    }
}