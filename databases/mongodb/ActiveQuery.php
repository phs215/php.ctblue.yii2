<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 5/29/2019
 * Time: 12:48 PM
 */

namespace ctblue\yii2\databases\mongodb;


class ActiveQuery extends \yii\mongodb\ActiveQuery
{
    public function andWhereBooleanSearch($model, $column)
    {
        if ($model->{$column}) {
            $this->andWhereIsTrue($column);
        } else if ($model->{$column} === '0') {
            $this->andWhereIsFalse($column);
        }
    }

    public function andWhereIntegerSearch($model, $column)
    {
//        var_dump($model->{$column});
//        exit;
        return $this->andWhere(['or', [$column => intval($model->{$column})], [$column => $model->{$column}]]);
    }

    public function andWhereIsTrue($column)
    {
        return $this->andWhere(['or', [$column => true], [$column => '1'], [$column => 1]]);
    }

    public function andWhereIsFalse($column)
    {
        return $this->andWhere(['or', [$column => false], [$column => '0'], [$column => 0]]);
    }
}