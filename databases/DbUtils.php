<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 16-Jan-20
 * Time: 3:50 PM
 */

namespace ctblue\yii2\databases;


use yii\db\Connection;

class DbUtils
{
    /**
     * get the db name as string from yii2
     * @param Connection
     * @return false|null|string
     * @throws \yii\db\Exception
     */
    public static function getDbName($db)
    {
        $database = $db->createCommand("SELECT DATABASE()")->queryScalar();
        return $database;
    }

    public static function execute($db, $cmd)
    {
        if (is_array($cmd)) {
            foreach ($cmd as $c) {
                \Yii::$app->db->createCommand($c)->execute();
            }
        } else {
            \Yii::$app->db->createCommand($cmd)->execute();
        }
    }
}