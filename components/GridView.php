<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 9/24/2018
 * Time: 1:20 PM
 */

namespace ctblue\yii2\components;


class GridView extends \kartik\grid\GridView
{
    function __construct(array $config = [])
    {
//        var_dump($config);
        //add the boolean filters
//        exit;
        parent::__construct($config);
    }

    public static function getBooleanFilter()
    {
        return [1 => 'Active', 0 => 'Inactive'];
    }
}