<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 9/24/2018
 * Time: 4:40 PM
 */

namespace ctblue\yii2\components;


class CSRF
{
    public static function asHiddenInput()
    {
        ob_start();
        ?>
        <input type="hidden" name="<?= \Yii::$app->request->csrfParam; ?>"
               value="<?= \Yii::$app->request->csrfToken; ?>"/>
        <?php
        return ob_get_clean();
    }
}