<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12-Nov-19
 * Time: 2:20 PM
 */

namespace ctblue\yii2\components\Bootstrap;

use ctblue\web\Utils\DateTimeUtils;
use yii\bootstrap\Modal;

class Bootstrap3Modal
{
    private static $showModalAdded = false;

    public static function drawModal($size = 'md')
    {
        $timestamp = uniqid();
        $modalId = 'modal-id-' . $timestamp;
        Modal::begin([
            'header' => '<span class="modal-header-text"></span>',
            'id' => $modalId,
            'size' => 'modal-' . $size,
            'closeButton'   => [
                'tag'   => 'button',
                'label' => '<span aria-hidden="true">×</span>'
            ],
        ]);
        Modal::end();
        if (!Bootstrap3Modal::$showModalAdded) {
            ?>
            <script>
                function showModal(modalId, title, text) {
                    $('.modal-backdrop').each(function(){
                        $(this).remove();
                    });
                    $('#modal-id-' + modalId).find('.modal-header-text').html(title);
                    $('#modal-id-' + modalId).find('.modal-body').html(text);
                    $('#modal-id-' + modalId).modal('show');
                    $(".modal").on("shown.bs.modal", function () {
                        if ($(".modal-backdrop").length > 1) {
                            $(".modal-backdrop").not(':first').remove();
                        }
                    })
                }
                function hideModal(modalId){
                    $('.modal-backdrop').each(function(){
                        $(this).remove();
                    });
                    $('#modal-id-' + modalId).find('.modal-header-text').html('');
                    $('#modal-id-' + modalId).find('.modal-body').html('');
                    $('#modal-id-' + modalId).modal('hide');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                }
            </script>
            <?php
            Bootstrap3Modal::$showModalAdded = true;
        }
        return $timestamp;
    }
}