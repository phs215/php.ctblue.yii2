<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 30-Aug-18
 * Time: 2:25 PM
 */

namespace ctblue\yii2\components;


class Recaptcha
{
    private $secret = '';

    function __construct($secret)
    {
        $this->secret = $secret;
    }

    public function parse()
    {
        $response = \Yii::$app->request->post("g-recaptcha-response");
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $data = array(
            'secret' => $this->secret,
            'response' => $response
        );
        $options = array(
            'http' => array(
                'method' => 'POST',
                'content' => http_build_query($data),
                'header' =>
                    "Content-Type: application/x-www-form-urlencoded\r\n"
            )
        );
        $context = stream_context_create($options);
        $verify = file_get_contents($url, false, $context);
        $captcha_success = json_decode($verify);
        return $captcha_success;
    }

}