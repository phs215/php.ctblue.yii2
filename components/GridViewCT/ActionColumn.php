<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 12/8/2019
 * Time: 12:53 PM
 */

namespace ctblue\yii2\components\GridViewCT;


class ActionColumn extends \kartik\grid\ActionColumn
{
    public $template = '{update} {delete}';

    public static function output()
    {
        return [
            'class' => ActionColumn::class
        ];
    }
}