<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 12/7/2019
 * Time: 11:03 AM
 */

namespace ctblue\yii2\components;


use ctblue\yii2\assets\JQueryUIAssetBundle;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use brussens\bootstrap\select\Widget as Select;

class ActiveForm extends \yii\widgets\ActiveForm
{
    public function autoCompleteBootstrapSelect($model, $sourceData, $attribute, $label = false)
    {
        $field = $this->field($model, $attribute)->widget(Select::className(), [
            'options' => ['data-live-search' => 'true'],
            'items' => $sourceData
        ]);
        if ($label) {
            $field->label($label);
        }
        return $field;
    }

    public function autoCompleteJUI($fieldName, $sourceData, $label = null, $defaultValue = null)
    {
        ob_start();
        $id = uniqid();
        $downId = 'down-' . $id;
        $comboId = 'combo-' . $id;
        ?>
        <div class="form-group autocomplete">
            <label class="control-label" for="<?= $fieldName ?>"><?= $label ?></label>
            <div class="input-group">
                <?php
                if ($label) {
                    ?>
                    <input id="<?= $comboId ?>" type="text" name="<?= $fieldName ?>" placeholder="<?= $label ?>"
                           class="form-control" autocomplete="off" value="<?= $defaultValue ?>">
                    <?php
                }
                ?>
                <div class="input-group-btn">
                    <a class="btn btn-default" id="btn-all-<?= $id ?>" href="javascript:void(0)">
                        <i class="fas fa-angle-down"></i>
                    </a>
                </div>
            </div>
        </div>
        <script>
            $(function () {
                var values<?= $id?>= [<?php
                    $i = 0;
                    $c = count($sourceData);
                    foreach ($sourceData as $key => $value) {
                        echo '"' . $value . '"';
                        if ($i < $c - 1) echo ',';
                        $i++;
                    }
                    ?>];
                autocomplete(document.getElementById("<?= $comboId?>"), values<?= $id?>, document.getElementById('btn-all-<?= $id?>'));
            });
        </script>
        <?php
        $a = ob_get_clean();
        return $a;
    }
}