<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 11/2/2019
 * Time: 12:53 PM
 */

namespace ctblue\yii2\components;

use ctblue\web\Utils\ImageUtil;
use ctblue\web\Utils\yii2\YiiUrlUtil;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\web\UploadedFile;


trait BaseFileUploader
{
    public $allowedExtensions = false;
    public $maxFileSizeMb = 3;

    public static function rootDirectory()
    {
        $directory = \Yii::getAlias('@webroot') . '/upload';
        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }
        return $directory;
    }

    public function upload($fileindex, $model, $userIdentifier)
    {
        $modelName = (new \ReflectionClass($this))->getShortName();
//        echo $modelName;
//        var_dump($_FILES);
//        exit;
        if (\Yii::$app->request->isPost
            && isset($_FILES[$modelName])
            && isset($_FILES[$modelName]['name'])
            && isset($_FILES[$modelName]['name']['file' . $fileindex])
            && $_FILES[$modelName]
            && $_FILES[$modelName]['name']
            && $_FILES[$modelName]['name']['file' . $fileindex]
        ) {
//            var_dump($_FILES);
//            exit;
            $this->{'file' . $fileindex} = UploadedFile::getInstance($this, 'file' . $fileindex);
//            var_dump($this->{'file' . $fileindex});
//            exit;
            if ($this->{'file' . $fileindex}) {
                if ($this->validateFile($fileindex)) {
                    //move the file
                    $this->{'hashed_file' . $fileindex} = $this->uploadFilename($fileindex);
                    $this->{'original_file' . $fileindex} = $this->{'file' . $fileindex}->name;
                    if (move_uploaded_file($this->{'file' . $fileindex}->tempName, $this->fullPath($userIdentifier, $fileindex))) {
                        $this->{'file' . $fileindex} = $this->fullPath($userIdentifier, $fileindex);
                        return true;
                    }
                }
            }
            $this->{'file' . $fileindex} = null;
            $this->{'hashed_file' . $fileindex} = null;
            $this->{'original_file' . $fileindex} = null;
        }
        return false;
    }

    public function drawImageInput($index, $directoryKey, $label = null)
    {
        if (!$label) $label = 'Image ' . $index;
        $modelName = (new \ReflectionClass($this))->getShortName();
        if ($this->{'file' . $index}) {
            ?>
            <img src="<?= $this->url($directoryKey, $index) ?>" id="image_preview<?= $index ?>"
                 style="max-width:200px;max-height:200px;"/>
            <button class="btn-primary btn"
                    onclick="$('#map-image-wrapper<?= $index ?>').show();$(this).hide();$('#image_preview<?= $index ?>').hide();$('#image_hidden<?= $index ?>').remove();$('#image_browser<?= $index ?>').attr('name','<?= $modelName ?>[<?= 'file' . $index ?>]');return false;">
                Change Image
            </button>
            <input type="hidden" name="<?= $modelName ?>[<?= 'file' . $index ?>]"
                   value="<?= $this->{'file' . $index} ?>"
                   id="image_hidden<?= $index ?>"/>
            <div style="<?php if ($this->{'file' . $index}) echo 'display:none' ?>" id="map-image-wrapper<?= $index ?>">
                <?php
                // Usage without a model
                echo '<label class="control-label">' . $label . '</label>';
                echo FileInput::widget([
                    'id' => 'image_browser' . $index,
                    'name' => 'image_browser' . $index,
                    'model' => $this,
                ]);
                ?>
            </div>
            <?php echo Html::a("Remove Image", ['remove-image', 'id' => $this->id, 'index' => $index], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => "Are you sure you want to remove this image?",
                ],
            ]) ?>
            <?php
        } else {
            // Usage without a model
            echo '<label class="control-label">' . $label . '</label>';
            echo FileInput::widget([
                'name' => $modelName . '[file' . $index . ']',
                'model' => $this,
            ]);
        }
    }

    private function validateFile($fileIndex, $allowedExtensions = false, $maxFileSizeMb = 3)
    {
        if ($this->{'file' . $fileIndex}) {
            $unacceptableFiles = ['exe', 'dll', 'php', 'cgi'];
            if (in_array($this->{'file' . $fileIndex}->extension, $unacceptableFiles)) {
                $this->addError('file', 'Invalid extension');
                return false;
            }
            if ($this->allowedExtensions && is_array($this->allowedExtensions)) {
                if (!in_array(strtolower($this->{'file' . $fileIndex}->extension), $this->allowedExtensions)) {
                    $this->addError('file', 'Invalid extension only these are allowed: ' . implode(',', $this->allowedExtensions));
                    return false;
                }
            }
//            var_dump($this->{'file'.$fileIndex}->size);
//            var_dump($this->maxFileSizeMb * 1024 * 1024);
//            exit;
            if ($this->{'file' . $fileIndex}->size > $this->maxFileSizeMb * 1024 * 1024) {
                $this->addError('file', 'Size is too large (max. ' . $this->maxFileSizeMb . 'MB)');
                return false;
            }
        }
        return true;
    }

    public function uploadFilename($fileIndex)
    {
        return uniqid() . microtime() . '.' . $this->{'file' . $fileIndex}->extension;
    }

    public function fullPath($directoryKey, $fileIndex)
    {
        $filename = $this->uploadDirectory($directoryKey) . '/' . $this->{'hashed_file' . $fileIndex};
//        echo $filename;exit;
        return $filename;
    }

    public function baseUrl($directoryKey, $appLocation = 'backend')
    {
        $url = '';
        if ($appLocation == 'backend') {
            $url = YiiUrlUtil::getBackendWebUrl();
        } else {
            $url = YiiUrlUtil::getFrontEndWebUrl();
        }
        return $url . '/upload/' . urlencode(urlencode(md5($directoryKey)) . base64_encode(crypt($directoryKey, 'if238jkdksm')));
    }

    public function url($directoryKey, $fileIndex, $appLocation = 'backend')
    {
        if (!$this->{'original_file' . $fileIndex}) return false;
        if (!$this->fileExists($directoryKey, $fileIndex)) return false;
        $url = $this->baseUrl($directoryKey, $appLocation) . '/'
            . $this->{'hashed_file' . $fileIndex};
        return $url;
    }

    public function fileExists($directoryKey, $fileIndex)
    {
        return is_file($this->fullPath($directoryKey, $fileIndex));
    }

    public function drawThumbnail($directoryKey, $fileIndex, $width, $height, $quality, $exactSize=false)
    {
        if ($this->fileExists($directoryKey, $fileIndex)) {
            if ($path = $this->fullPath($directoryKey, $fileIndex)) {
                $imageUtil = new ImageUtil($this->fullPath($directoryKey, $fileIndex));
                $imageUtil->resizeImage($width, $height, ($exactSize)?'exact':'auto');
                return $imageUtil->saveImage('.png', $quality, true);
            }
        }
        return false;
    }

    /**
     * Outputs the audio as HTML5 tag
     * @param $appLocation
     * @param $directoryKey
     * @param $fileIndex
     * @return bool|false|string
     */
    public function drawAudio($directoryKey, $fileIndex, $appLocation = 'backend')
    {
        if ($this->fileExists($directoryKey, $fileIndex)) {
            if ($path = $this->fullPath($directoryKey, $fileIndex)) {
                $audioPath = $this->fullPath($directoryKey, $fileIndex);
                $audioUrl = $this->url($directoryKey, $fileIndex, $appLocation);
                ob_start();
                ?>
                <audio controls>
                    <source src="<?= $audioUrl ?>" type="audio/mpeg">
                    Your browser does not support the audio element.
                </audio>
                <?php
                $html = ob_get_clean();
                return $html;
            }
        }
        return false;
    }

    public function uploadDirectory($directoryKey)
    {
        $rootDir = BaseFileUploader::rootDirectory();
        $uploadDirectory = $rootDir . '/' . md5($directoryKey) . base64_encode(crypt($directoryKey, 'if238jkdksm'));
        if (!is_dir($uploadDirectory)) {
            mkdir($uploadDirectory, 0777, true);
        }
        return $uploadDirectory;
    }

    /**
     * Downloads the file
     * @return bool
     */
    public function download($directoryKey, $fileIndex)
    {
        $file = $this->fullPath($directoryKey, $fileIndex);
        $str = file_get_contents($file);
//        var_dump($str);exit;
        header('Content-Disposition: attachment; filename="' . $this->{'original_file' . $fileIndex} . '"');
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header('Content-Length: ' . strlen($str));
        header('Connection: close');
        echo $str;
        return true;
    }

    public function deleteAttachment($index, $userIdentifier)
    {
        if ($this->{'hashed_file' . $index}) {
            unlink($this->fullPath($userIdentifier, $index));
            $this->{'hashed_file' . $index} = '';
            $this->{'original_file' . $index} = '';
            $this->{'file' . $index} = '';
        }
    }

}
