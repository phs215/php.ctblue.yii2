<?php

namespace ctblue\yii2\components;


class JUIAutocomplete
{
    public $sortData = true;
    public $storeValue = true;
    public $overrideDisplayValue = false;
    public $displayCount = 10;
    public $showDropDown = true;
    public $allowCustomData = true;

    public function field($model, $variable, $variableClass, $title, $data)
    {
        if ($this->sortData) {
            asort($data);
        }
        $value = $model->$variable;
        if ($this->overrideDisplayValue) $value = $this->overrideDisplayValue;
        ?>
        <div class="form-group field-<?= $variable ?> validating">
            <label for="label-<?= $variable ?>"><?= $title ?></label>
            <div class="form-group autocomplete">
                <div class="input-group">
                    <input id="combo-<?= $variable ?>-hidden-key" type="hidden"
                           placeholder="" class="form-control" autocomplete="off" value="<?= $model->$variable ?>">
                    <input id="combo-<?= $variable ?>-hidden" type="hidden"
                           name="<?= $variableClass ?>[<?= $variable ?>]"
                           placeholder="" class="form-control" autocomplete="off" value="<?= $model->$variable ?>">
                    <input id="combo-<?= $variable ?>" type="text"
                           placeholder="" class="form-control" autocomplete="off" value="<?= $value ?>">
                    <?php
                    if ($this->showDropDown) {
                        ?>
                        <div class="input-group-btn">
                            <a class="btn btn-default" id="btn-all-input-<?= $variable ?>" href="javascript:void(0)">
                                <i class="fas fa-angle-down"></i>
                            </a>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <script>
                $(function () {
                    autocomplete($("#combo-<?= $variable ?>"),
                        <?= json_encode($data) ?>, $("#btn-all-input-<?= $variable ?>"), function (v, key) {
                            $("#combo-<?= $variable ?>").val(v);
                            $("#combo-<?= $variable ?>-hidden-key").val(key);
                            <?php
                            if($this->storeValue){
                            ?>
                            $("#combo-<?= $variable ?>-hidden").val(v);
                            <?php
                            }else{
                            ?>
                            $("#combo-<?= $variable ?>-hidden").val(key);
                            <?php
                            }
                            ?>
                        },<?= $this->displayCount ?>, function () {
                            <?php
                            if(!$this->storeValue && !$this->allowCustomData){
                            ?>
                            let key = $("#combo-<?= $variable ?>-hidden-key").val();
                            if (!key) {
                                $("#combo-<?= $variable ?>-hidden").val('');
                                $("#combo-<?= $variable ?>").val('');
                            }
                            <?php
                            }
                            ?>
                        });
                });
            </script>
        </div>
        <?php
    }

    public static function draw($model, $variable, $variableClass, $title, $data, $sortData = true, $storeValue = true, $overrideDisplayValue = false, $displayCount = 10, $showDropDown = true)
    {
        if ($sortData) {
            asort($data);
        }
        $value = $model->$variable;
        if ($overrideDisplayValue) $value = $overrideDisplayValue;
        ?>
        <div class="form-group field-<?= $variable ?> validating">
            <label for="label-<?= $variable ?>"><?= $title ?></label>
            <div class="form-group autocomplete">
                <div class="input-group">
                    <input id="combo-<?= $variable ?>-hidden" type="hidden"
                           name="<?= $variableClass ?>[<?= $variable ?>]"
                           placeholder="" class="form-control" autocomplete="off" value="<?= $model->$variable ?>">
                    <input id="combo-<?= $variable ?>" type="text"
                           placeholder="" class="form-control" autocomplete="off" value="<?= $value ?>">
                    <?php
                    if ($showDropDown) {
                        ?>
                        <div class="input-group-btn">
                            <a class="btn btn-default" id="btn-all-input-<?= $variable ?>" href="javascript:void(0)">
                                <i class="fas fa-angle-down"></i>
                            </a>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <script>
                $(function () {
                    autocomplete($("#combo-<?= $variable ?>"),
                        <?= json_encode($data) ?>, $("#btn-all-input-<?= $variable ?>"), function (v, key) {
                            $("#combo-<?= $variable ?>").val(v);
                            <?php
                            if($storeValue){
                            ?>
                            $("#combo-<?= $variable ?>-hidden").val(v);
                            <?php
                            }else{
                            ?>
                            $("#combo-<?= $variable ?>-hidden").val(key);
                            <?php
                            }
                            ?>
                        },<?= $displayCount ?>);
                });
            </script>
        </div>
        <?php
    }
}