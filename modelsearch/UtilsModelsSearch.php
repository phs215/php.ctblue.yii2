<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 8/27/2018
 * Time: 10:20 PM
 */

namespace ctblue\yii2\modelsearch;


class UtilsModelsSearch
{
    public static function isSorted()
    {
        return \Yii::$app->request->get('sort');
    }
}