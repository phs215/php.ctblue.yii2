<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 8/27/2018
 * Time: 1:24 PM
 */

namespace ctblue\yii2\controllers;


use ctblue\yii2\models\UserCT;
use yii\console\Controller;
use yii\helpers\Console;
use yii\web\User;

class UserConsoleController extends Controller
{

    public function actionSetPassword($email, $password)
    {
        if ($user = UserCT::findOne(['email' => $email])) {
            $user->setPassword($password);
            echo "Password has been changed for user with email $email";
        } else {
            echo "User could not be found";
        }
    }

    public function actionDelete($userId){
        if(UserCT::deleteAll(['id'=>$userId])){
            echo "User has been deleted\r\n";
        }else{
            echo "We could not delete the user\r\n";
        }
    }
    public function actionCreateUser($email, $password)
    {
        if ($user = UserCT::findOne(['email' => $email])) {
            echo "Error: user email $email already exists\r\n";
        } else {
            $user = new UserCT();
            if(!isset($user->username)){
                $user->username = $email;
            }
            $user->auth_key = '123456';
            $user->password_hash = '123456';
            $user->email = $email;
            $user->status = 10;
            $user->insert(false);
            $user->setPassword($password);
            $user->save();
            echo "Password has been created\r\n";
        }
    }

    public function actionActivate($email)
    {
        if ($user = UserCT::findOne(['email' => $email])) {
            $user->status = 10;
            $user->save();
            echo "User with email $email has been activated";
        } else {
            echo "User could not be activated";
        }
    }

    public function actionDeactivate($email)
    {
        if ($user = UserCT::findOne(['email' => $email])) {
            $user->status = 0;
            $user->save();
            echo "User with email $email has been deactivated";
        } else {
            echo "User could not be deactivated";
        }
    }

    public function actionList()
    {
        $users = UserCT::find()->all();
        foreach ($users as $user) {
            $this->stdout("ID: $user->id - email: $user->email - status: $user->status\r\n");
        }
    }

    public function actionChangeEmail($id, $email)
    {
        if($user=UserCT::findOne($id)){
            $user->email=$email;
            if($user->update()){
                $this->stdout("Changed email to $email", Console::FG_RED."\r\n");
                return;
            }
        }
        $this->stdout("Could not change email", Console::BG_RED."\r\n");
    }
}