<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 12/7/2019
 * Time: 12:30 PM
 */

namespace ctblue\yii2\controllers;


use yii\db\ActiveRecord;
use yii\web\Controller;
use Yii;

class WebBaseController extends Controller
{
    public $primaryKey = 'id';
    protected $redirectToIndexOnSave = false;

    protected function redirectToIndex()
    {
        return $this->redirect('index');
    }

    protected function modelClass()
    {
        throw new \Exception("Not implemented");
    }

    protected function findModel($id)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * @param $message
     * @param ActiveRecord|bool $debugModels,... unlimited OPTIONAL
     */
    public function sessionError($message)
    {
        $a = '';
        $debugModels = func_get_args();
        for ($i = 1; $i < count($debugModels); $i++) {
            $debugModel = $debugModels[$i];
            if ($debugModel) {
                ob_start();
                var_dump($debugModel->errors);
                $a = ' ' . ob_get_clean();
            }
        }
        $this->sessionBootstrapAlert('error', $message . $a);
    }

    public function sessionSuccess($message)
    {
        $this->sessionBootstrapAlert('success', $message);
    }

    public function sessionWarning($message)
    {
        $this->sessionBootstrapAlert('warning', $message);
    }

    public function sessionInfo($message)
    {
        $this->sessionBootstrapAlert('info', $message);
    }

    private function sessionBootstrapAlert($type, $message)
    {
        Yii::$app->session->setFlash($type, $message);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->delete()) {
            Yii::$app->session->setFlash('success', "The record has been deleted");
        } else {
            Yii::$app->session->setFlash('error', "An error occurred while trying to delete the record");
            if (YII_DEBUG) {
                var_dump($model->errors);
                exit;
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Creates a new MeetingBackend model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
//        var_dump($this->modelClass());exit;
        $className = $this->modelClass();
        $model = new $className;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'The record has been created.');
                if ($this->redirectToIndexOnSave) {
                    return $this->redirect([
                        'index',
                    ]);
                } else {
                    return $this->redirect([
                        'update', 'id' => $model->{$this->primaryKey},
                    ]);
                }
            } else {
                if ($model->hasErrors('file')) {
                    ob_start();
                    ?>
                    We've encountered an error while trying to upload the file. Please check that:<br>
                    1. The maximum size of the file is <?= $model->maxFileSizeMb ?>Mb<br>
                    <?php
                    if ($model->allowedExtensions && count($model->allowedExtensions) > 0) {
                        ?>
                        2. The extension of the file is one of <?= implode(', ', $model->allowedExtensions) ?>
                        <?php
                    }
                    ?>
                    <?php
                    $error = ob_get_clean();
                    $this->sessionError($error);
                } else {
                    $this->sessionError('Could not create the record');
                    if (YII_DEBUG) {
                        var_dump($model->errors);
                        exit;
                    }
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MeetingBackend model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'The record has been updated.');
                if ($this->redirectToIndexOnSave) {
                    return $this->redirect([
                        'index',
                    ]);
                } else {
                    return $this->redirect([
                        'update', 'id' => $model->{$this->primaryKey},
                    ]);
                }
            } else {
                if ($model->hasErrors('file')) {
                    ob_start();
                    ?>
                    We've encountered an error while trying to upload the file. Please check that:<br>
                    1. The maximum size of the file is <?= $model->maxFileSizeMb ?>Mb<br>
                    <?php
                    if ($model->allowedExtensions && count($model->allowedExtensions) > 0) {
                        ?>
                        2. The extension of the file is one of <?= implode(', ', $model->allowedExtensions) ?>
                        <?php
                    }
                    ?>
                    <?php
                    $error = ob_get_clean();
                    $this->sessionError($error);
                } else {
                    $this->sessionError('Could not update the record');
                    if (YII_DEBUG) {
                        var_dump($model->errors);
                        exit;
                    }
                }
            }
            return $this->redirect(['update', 'id' => $model->{$this->primaryKey}]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
}
