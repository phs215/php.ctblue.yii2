<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 11/24/2019
 * Time: 10:14 PM
 */

namespace ctblue\yii2\controllers;


use common\modelsCommon\UserCommon;
use common\modelsCommon\UserTokenCommon;
use ctblue\web\api\ApplicationRequest;
use ctblue\web\api\ApplicationResponse;
use yii\rest\Controller;

class ApiBaseController extends Controller
{
    /**
     * @var ApplicationRequest
     */
    protected $apiRequest = null;

    /**
     * @var ApplicationResponse
     */
    protected $apiResponse = null;

    public $csrf = false;

    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $this->apiRequest = new ApplicationRequest();

        $this->apiResponse = new ApplicationResponse();

        if (!$this->csrf) {
            $this->enableCsrfValidation = false;
        }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }

    /**
     * @return UserCommon
     */
    protected function loginWithToken()
    {
        return $this->verifyToken($this->request->getValue('token'));
    }

    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to
                    'Origin' => ['*'],
                    'Access-Control-Allow-Origin' => ["*"],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Method' => ['POST', 'PUT', "GET"],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Request-Headers' => ['*'],
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],

            ],
        ];
    }

    /**
     * @param $token
     * @return bool|UserCommon
     */
    public function verifyToken($token)
    {
        //check if the user exists in the generator section. if it does than set genrator parameters
        if ($a = UserCommon::findBySql("SELECT u.* FROM user_token as t, user as u WHERE u.id=t.user_id AND t.token=:token", [':token' => $token])->one()) {
            return $a;
        }
        return false;
    }
}
