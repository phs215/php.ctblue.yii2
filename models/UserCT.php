<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 8/27/2018
 * Time: 11:48 AM
 */

namespace ctblue\yii2\models;


use common\models\User;
use ctblue\web\Utils\yii2\HttpUtils;
use yii\helpers\Url;

class UserCT extends User
{
    public $password;
    public $rememberMe;

    public function isGuest()
    {
        if ($this->status == 10 && $this->email == Userext::getGuestEmail()) return true;
    }

    /**
     * login with post data or serial number
     * @param $modelName string
     * @return UserCT|bool
     */
    public static function loginPost($modelName='Userext')
    {
        if ($loginData = \Yii::$app->request->post($modelName)) {
            if (isset($loginData['email'])) {
                $user = new UserCT();
                $user->email = $loginData['email'];
                if (isset($loginData['password'])) {
                    $user->password_hash = $loginData['password'];
                    if (UserCT::login($user->email, $user->password_hash, HttpUtils::post($modelName,'rememberMe'))) {
                        return true;
                    } else {
                        \Yii::$app->session->setFlash('danger', 'Login failed');
                        return false;
                    }
                } else {
                    \Yii::$app->session->setFlash('danger', 'Password is missing');
                }
                return $user;
            } else if (isset($loginData['serialNumber'])) {
                if ($user = UserCT::loginBySerialNumber($loginData['serialNumber'])) {
                    UserCT::setSessionInformation($user, \Yii::$app->request->post('rememberMeSerial'));
                    return $user;
                }
            }
        }
        return false;
    }

    /**
     * login to the server without setting any session parameters
     * @param $email
     * @param $password
     * @return bool|Userext
     */
    public static function loginNoSession($email, $password)
    {
        if ($model = UserCT::findOne(['email' => $email, 'status' => UserStatus::ACTIVE])) {
            if (password_verify($password, $model->password_hash)) {
                return $model;
            }
        }
        return false;
    }

    public static function login($email, $password, $rememberMe)
    {
        if ($model = UserCT::findOne(['email' => $email, 'status' => UserStatus::ACTIVE])) {
            if (password_verify($password, $model->password_hash)) {
                UserCT::setSessionInformation($model, $rememberMe);
                return true;
            }
        }
        return false;
    }

    /**
     * @param $user UserCT
     * @param $rememberMe bool
     */
    public static function setSessionInformation($user, $rememberMe)
    {
        if ($rememberMe) {
            \Yii::$app->session->setTimeout(99999999);
        }
        \Yii::$app->session->set('user', $user);
    }

    public static function isLoggedIn()
    {
        if (\Yii::$app->session->get('user')) return true;
        return false;
    }


    /**
     * @return UserCT
     */
    public static function getLoggedInUser()
    {
        return \Yii::$app->session->get('user');
    }

    /**
     * get the id of the user that's currently logged in
     * @return bool|int
     */
    public static function getUserId()
    {
        if ($user = UserCT::getLoggedInUser()) {
            return $user->id;
        }
        return false;
    }
    /**
     * get the id of the user that's currently logged in
     * @return bool|int
     */
    public static function getUsername()
    {
        if ($user = UserCT::getLoggedInUser()) {
            return $user->username;
        }
        return false;
    }

    public static function logout()
    {
        \Yii::$app->session->remove('user');
    }

    public static function loginRequired()
    {
        if (!UserCT::isLoggedIn()) {
            UserCT::redirectToLoginPage();
            exit;
        }
    }

    public static function redirectToLoginPage($logout = false, $loginPageRoute = '/site/login')
    {
        if ($logout) {
            UserCT::logout();
        }
        header('Location:' . Url::toRoute($loginPageRoute), true, 302);
    }

    public function setPassword($password)
    {
        $this->password_hash = password_hash($password, PASSWORD_DEFAULT);
        return $this->save();
    }
    public static function generateToken($length = 50)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[UserCT::crypto_rand_secure(0, $max - 1)];
        }
        $token .= str_replace('.','',uniqid('',true)).bin2hex(random_bytes(30));

        return $token;
    }
    private static function crypto_rand_secure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int)($log / 8) + 1; // length in bytes
        $bits = (int)$log + 1; // length in bits
        $filter = (int)(1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }
}
