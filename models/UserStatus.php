<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 8/30/2018
 * Time: 2:07 PM
 */

namespace ctblue\yii2\models;


class UserStatus
{
    const ACTIVE=10;
    const NOT_ACTIVE=-1;
    const BANNED=20;

    public static function toHashTable()
    {
        return [UserStatus::ACTIVE => 'Active', UserStatus::NOT_ACTIVE => 'Not Active', UserStatus::BANNED => 'Banned'];
    }

    public static function toString($status)
    {
        $arr = self::toHashTable();
        if (isset($arr[$status])) return $arr[$status];
        return false;
    }
}