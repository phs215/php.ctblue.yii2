<?php
/**
 * Created by PhpStorm.
 * User: Philippe S
 * Date: 11/17/2018
 * Time: 8:23 PM
 */

namespace ctblue\yii2\models;


use yii\db\ActiveRecord;

class ActiveRecordHelper
{
    /**
     * Sets a field as required
     * @param $oldRules
     * @param array $cols
     * @return mixed
     */
    public static function addRequiredRules($oldRules, $cols = [])
    {
        for ($i = 0; $i < count($oldRules); $i++) {
            if ($oldRules[$i][1] == 'required') {
                $oldRules[$i][0] = array_merge($oldRules[$i][0], $cols);
            }
        }
        return $oldRules;
    }

    /**
     * Shows all the models error in a session error flash
     * @param $model ActiveRecord
     */
    public static function setModelsErrorsAsSessions($model)
    {
        if ($model->errors) {
            if (count($model->errors) > 0) {
                ob_start();
                var_dump($model->errors);
                $a = ob_get_clean();
                \Yii::$app->session->setFlash('error', $a);
            }
        }
    }
}